package example_plateau;

import com.junit.tasks.example_plateau.Main;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class PlateauTest {
    @Test
    public void testLargestPlateau() {
        Main largestPlateau = new Main();
        int[] LONGEST_PLATEAU_IS = new int[]{1, 9, 3, 10, 4, 20, 2};
        String value = String.valueOf(largestPlateau.findLongestPlateauIn(LONGEST_PLATEAU_IS));
        assertEquals(value.equals("RealPlataeu [startIndex=3, value=10, consecutiveValueCount=1]"), "ok");
        System.out.println();
    }

    @Test
    public void test2LargestPlateau() {
        Main largestPlateau = new Main();
        int[] LONGEST_PLATEAU_IS = new int[]{1, 2, 3, 3, 3, 2, 5, 5, 4};
        String value = String.valueOf(largestPlateau.findLongestPlateauIn(LONGEST_PLATEAU_IS));
        assertEquals(value.equals("RealPlataeu [startIndex=3, value=10, consecutiveValueCount=1]"), "ok");
        System.out.println(value);
    }
}
